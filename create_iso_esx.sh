#!/bin/bash

if [ ! "$1" ]; then echo USGAE: ./build.sh [hostname] [ip] [subnet] [gateway]; exit ; fi
if [ ! "$2" ]; then echo USGAE: ./build.sh [hostname] [ip] [subnet] [gateway]; exit ; fi
if [ ! "$3" ]; then echo USGAE: ./build.sh [hostname] [ip] [subnet] [gateway]; exit ; fi

mkdir -p ./config/$1
cp ./config/template_esx/* ./config/$1
sed -i '' -e "s/HOST/$1/g" ./config/$1/meta-data
sed -i '' -e "s/IP/$2/g" ./config/$1/meta-data
sed -i '' -e "s/SUBNET/$3/g" ./config/$1/meta-data
sed -i '' -e "s/GATEWAY/$4/g" ./config/$1/meta-data

rm $1.iso
hdiutil makehybrid -hfs -joliet -iso -default-volume-name cidata -o $1.iso ./config/$1

scp ~/cloudinit/$1.iso root@172.16.0.4:/vmfs/volumes/disk1/$1.iso
