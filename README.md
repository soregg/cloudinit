# Deploy a Ubuntu cloud image to ESXi

## Requirement

`hdiutil` for MacOS

## Procedure

1. Download the Ubuntu Cloud image from from Ubuntu website.
2. Deploy the VM to ESX, DO NOT Start it.
3. Modify your credential for esx in create_iso_esx.sh 
4. Modify meta-data and user-data in template_esx.
5. Run create_iso_esx.sh. `example: /create_iso_esx.sh test123 172.20.0.123 255.255.255.0 172.20.0.181`
6. On ESX console, insert the new created ISO to the VM's CD-rom.
7. Turn on the VM, it will initialize the VM with your settings.

## Download link for Ubuntu 18 Cloud Image

https://cloud-images.ubuntu.com/bionic/current/

[Document and scripts to be implement.]
